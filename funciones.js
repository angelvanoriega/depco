function hideshow(valor, etiqueta) {
    if (valor === "") {
        $(etiqueta).hide("slow");
    } else {
        $(etiqueta).show(2000);
    }
}

$(document).ready(function () {
    document.getElementById('producto_titulo').value = "CALAVERA BMW SERIE 1 05-07 S/ARNES T DER";
    document.getElementById('producto_imagen').value = "https://cdn.shopify.com/s/files/1/1121/4854/products/c0a494e1557eb729af1e4028daf617a8_1024x1024.jpg";
    modificarImagen();
    modificarTitulo();
});

function modificarTitulo() {
    var valor = document.getElementById('producto_titulo').value;
    var alerta = document.getElementById('layout_titulo');
    alerta.innerHTML = valor;
    modificarImagen();
    hideshow(valor, "#layout_titulo");
}

function modificarImagen() {
    var valor = document.getElementById('producto_imagen').value;
    var img = new Image();
    img.onload = function () {
        var ascpect = (this.width / this.height);
        if (this.width === this.height) {
            document.getElementById('layout_imagen').width = 200;
            document.getElementById('layout_imagen').height = 200;
        } else {
            if (this.width > this.height) {
                document.getElementById('layout_imagen').width = ascpect * 200;
                document.getElementById('layout_imagen').height = 200;
            } else {
                document.getElementById('layout_imagen').width = 200;
                document.getElementById('layout_imagen').height = 200 / ascpect;
            }
        }
    };
    img.src = valor;
    if(valor !== "") {
        document.getElementById('layout_imagen').src = valor;
    }
    hideshow(valor, "#layout_imagen");
}

function modificarCondiciones() {
    var valor = document.getElementById('producto_condiciones').value;
    hideshow(valor, "#condiciones");
    var alerta = document.getElementById('layout_condiciones');
    alerta.innerHTML = valor;
}

function modificarGarantia() {
    var valor = document.getElementById('producto_garantia').value;
    hideshow(valor, "#garantias");
    var alerta = document.getElementById('layout_garantia');
    alerta.innerHTML = valor;
}

function modificarFabricante() {
    var valor = document.getElementById('producto_fabricante').value;
    hideshow(valor, "#fabricantes");
    var alerta = document.getElementById('layout_fabricante');
    alerta.innerHTML = valor;
}

function modificarMarcas() {
    var valor = document.getElementById('producto_marcas').value;
    hideshow(valor, "#marcas");
    var alerta = document.getElementById('layout_marcas');
    alerta.innerHTML = valor;
}

function modificarModelos() {
    var valor = document.getElementById('producto_modelos').value;
    hideshow(valor, "#modelos");
    var alerta = document.getElementById('layout_modelos');
    alerta.innerHTML = valor;
}

function modificarAnios() {
    var valor = document.getElementById('producto_anios').value;
    hideshow(valor, "#anios");
    var alerta = document.getElementById('layout_anios');
    alerta.innerHTML = valor;
}

function modificarInventario() {
    var valor = document.getElementById('producto_inventario').value;
    hideshow(valor, "#inventarios");
    var alerta = document.getElementById('layout_inventario');
    alerta.innerHTML = valor;
}

function modificarObservaciones() {
    var valor = document.getElementById('comentarios').value;
    hideshow(valor, "#observaciones");
    var alerta = document.getElementById('layout_comentarios');
    alerta.innerHTML = valor;
}

function validarRegistroUsuario() {
    var UsuarioNombre = document.getElementById('contenido-ml');
    alert(UsuarioNombre.outerHTML);
}

function Copytoclipboard() {
    var body = document.body,
            range, sel;
    if (document.createRange && window.getSelection) {
        range = document.createRange();
        sel = window.getSelection();
        sel.removeAllRanges();
        try {
            range.selectNodeContents("contenido-ml");
            sel.addRange(range);
            document.execCommand('Copy');
        } catch (e) {
            range.selectNode(el);
            sel.addRange(range);
            document.execCommand('Copy');
        }
    } else if (body.createTextRange) {
        range = body.createTextRange();
        range.moveToElementText(el);
        range.select();
        range.execCommand('Copy');
    }
}
