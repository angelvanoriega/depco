$(document).ready(function () {
    $('#descripcion').trumbowyg({
        autogrow: true,
        removeformatPasted: true,
        autogrowOnEnter: true,
        lang: 'es',
        prefix: 'custom-prefix'
    });
});

$('#descripcion').trumbowyg().on('tbwchange', function () {
    var valor = $('#descripcion').trumbowyg('html');
    var alerta = document.getElementById('layout_mensaje');
    alerta.innerHTML = valor;
});

$('#descripcion').trumbowyg().on('tbwinit', function () {
     var editor = document.getElementsByClassName('trumbowyg-editor');
     editor[0].innerHTML = "<p>Para hacer valida cualquiera de nuestras garantías es necesario contar con la nota del artículo, la cual viene adentro del paquete. De Igual forma en el paquete de su compra se agrega una etiqueta donde vienen nuestros datos en caso de que requieras enviarnos una devolución. El Cambio de los artículos procederá siempre y cuando el producto presente defectos de fabricación y no fallas ocasionadas por:&nbsp;</p><p><ol><li>Una mala instalación<br></li><li>Falla por inversión de polaridad<br></li><li>Golpes por caída<br></li><li>Alteración y modificación de cables</li></ol><p><br></p></p><p><strong>Garantía por tipo de artículos:</strong></p><p><strong>- Iluminación Automotriz:</strong></p><p><ol><li>En el caso de Calavera, Cuartos, Faros y Faros de Niebla otorgamos un año de garantía por filtración de agua a partir del día que se recibe el producto<br></li></ol></p><p><strong>- Espejos Laterales:</strong></p><p><ol><li>Un año de garantía por vibración de la luna del espejo o de la base, a partir del día que se recibe el producto<br></li><li>No aplica si presenta daños el espejo</li></ol></p><p><strong>- Resto de los artículos:</strong></p><p><ol><li>Todos los artículos no mencionados anteriormente, cuentan con un tiempo de reclamación de 24hrs después de haberlo recibido.<br></li></ol></p><br>";
});

function modificarTitulo() {
    var valor = document.getElementById('producto_titulo').value;
    var alerta = document.getElementById('layout_titulo');
    alerta.innerHTML = valor;
}